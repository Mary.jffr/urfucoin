
from django.http import *
from django.template import loader, RequestContext
from django.utils.translation import ugettext as _
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.shortcuts import render, redirect
from .models import Student


def page_index(request):
    context = {
        'request': request
    }
    template = loader.get_template("index.html")
    return HttpResponse(template.render(context))


def page_login(request):
    template = loader.get_template("login.html")
    return HttpResponse(template.render())


def proc_login(request):
    user_name = request.GET['user_name']
    password = request.GET['password']
    user = authenticate(username=user_name, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect('page_index')
        else:
            return redirect('page_login')
    return redirect('page_login')


def page_signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('page_login')
    else:
        form = UserCreationForm()
        return render(request, 'signup.html', {'form': form})


def page_profile(request):
    user_data = Student.objects.all
    return render(request, 'profile.html', {'user_data': user_data})


def password_change(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
        return redirect('page_profile')
    else:
        form = PasswordChangeForm(request.user)
        return render(request, 'password_change.html', {'form': form})


