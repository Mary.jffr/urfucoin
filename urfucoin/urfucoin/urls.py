"""urfucoin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^$', views.page_index, name="page_index"),

    # Администрирование
    url(r'^login/', views.page_login, name="page_login"),
    url(r'^proc_login/', views.proc_login, name="proc_login"),

    # Регистрация пользователя
    url(r'^signup/', views.page_signup, name="page_signup"),

    # Профиль пользователя
    url(r'^profile/', views.page_profile, name="page_profile"),

    # Изменение пароля пользователем
    url(r'^password_change/', views.password_change, name="password_change"),

    path('admin/', admin.site.urls),
]
