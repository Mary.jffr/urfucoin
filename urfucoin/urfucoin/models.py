# -*- coding: UTF-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Student(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    
    class Meta:
        db_table = "Student"

#Создание профиля пользователя при регистрации
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Student.objects.create(user=instance)

@receiver
def save_user_profile(sender, instance, **kwargs):
    instance.student.save()


class Events(models.Model):
    """
    Мероприятия
    """
    name = models.CharField(max_length=32)
    category = models.ForeignKey('EventCategories', null=True, on_delete=models.DO_NOTHING)
    description = models.CharField(max_length=254)
    venues = models.ForeignKey('Venues', null=True, on_delete=models.DO_NOTHING)
    dateEvent = models.DateField(null=False, blank=False)
    startTime = models.DateTimeField(null=False, blank=False)
    participant = models.ManyToManyField(User)
    coins = models.IntegerField(null=True)

    class Meta:
        db_table = "events"
        app_label = "urfucoin"


class Venues(models.Model):
    """
    Места проведения мероприятий
    """
    name = models.CharField(max_length=32)
    address = models.CharField(max_length=32)


class EventCategories(models.Model):
    """
    Категории мероприятий
    """
    name = models.CharField(max_length=32)
