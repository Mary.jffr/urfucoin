
from django.test import TestCase
from django.contrib.auth.models import User


class UsersLoginTest(TestCase):
    def setUp(self) -> None:
        test_user1 = User.objects.create_user(username='test_user1', email='user1@gmail.com', password='IamUSER1')
        test_user2 = User.objects.create_user(username='test_user2', email='user2@gmail.com', password='54321')
        test_admin = User.objects.create_user(username='test_admin', email='admin@gmail.com',
                                              password='IamTestAdmin', is_superuser=1, is_staff=1)

    def test_login_users(self):
        self.assertTrue(self.client.login(username='test_user1', password='IamUSER1'))
        self.assertFalse(self.client.login(username='test_user1', password='IamUSER'))
        self.assertTrue(self.client.login(username='test_user2', password='54321'))
        self.assertFalse(self.client.login(username='test_user2', password='544211'))

    def test_login_admin(self):
        self.client.login(username='test_admin', password='IamTestAdmin')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)


class PasswordChangeTest(TestCase):
    def setUp(self) -> None:
        test_user = User.objects.create_user(username='test_user', email='user@gmail.com', password='IamOldPasswd')
        test_admin = User.objects.create_user(username='test_admin', email='admin@gmail.com',
                                              password='IamOldPasswdAdmin', is_superuser=1, is_staff=1)

    def test_password_change_User(self):
        testuser = User.objects.get(username='test_user')
        testuser.set_password('IamNewPasswd')
        testuser.save()
        self.assertTrue(self.client.login(username='test_user', password='IamNewPasswd'))
        self.assertFalse(self.client.login(username='test_user', password='IamOldPasswd'))

    def test_password_change_Admin(self):
        testadmin = User.objects.get(username='test_admin')
        testadmin.set_password('IamNewPasswdAdmin')
        testadmin.save()
        self.client.login(username='test_admin', password='IamNewPasswdAdmin')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.client.login(username='test_admin', password='IamOldPasswdAdmin'))

